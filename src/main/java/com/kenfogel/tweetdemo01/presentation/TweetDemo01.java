package com.kenfogel.tweetdemo01.presentation;

import com.kenfogel.tweetdemo01.business.TwitterEngine;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

/**
 * Based on code from https://www.baeldung.com/twitter4j
 *
 * @author Ken Fogel
 */
public class TweetDemo01 extends Application {

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(TweetDemo01.class);
    
    /**
     * Where it begins
     * @param args 
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    private TextArea textArea;
    private final TwitterEngine twitterEngine = new TwitterEngine();
    private Button btn;
    
    /**
     * JavaFX begins at start
     * 
     * @param primaryStage
     * @throws Exception 
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        // Set window's title
        TweetMenu(primaryStage);
        
    }
    public void TweetMenu(Stage primaryStage){
        primaryStage.setTitle("Send a tweet");
        BorderPane root = new BorderPane();
        root.setBottom(makeButtons());
        
        textArea = new TextArea();
        textArea.setWrapText(true);
        root.setCenter(textArea);
        
        btn=new Button();
        btn.setText("Direct Messages");
        btn.setId("dm");
        EventHandler<ActionEvent> event = new EventHandler<ActionEvent>() { 
            public void handle(ActionEvent e) 
            { 
                DirectMessagesMenu(primaryStage);
            } 
        };
        btn.setOnAction(event);
        root.setTop(btn);
        
        
        primaryStage.setScene(new Scene(root, 300, 250));
        primaryStage.show();
    }
    public void DirectMessagesMenu(Stage primaryStage){
        primaryStage.setTitle("Direct Messages menu");
        BorderPane root = new BorderPane();
        
        TextField textField = new TextField();
        
        textField.setId("HandleInput");
        root.setTop(textField);        
         
        textArea = new TextArea();
        textArea.setWrapText(false);
        root.setCenter(textArea);
        
        Button btn =new Button();
        EventHandler<ActionEvent> event = new EventHandler<ActionEvent>() { 
            public void handle(ActionEvent e) 
            { 
                try{
                    twitterEngine.sendDirectMessage(textField.getText(),textArea.getText());
                }
                catch(Exception ex){
                    System.out.println(ex);
                }
                
            } 
        };
        btn.setOnAction(event);
        btn.setText("Dm");
        root.setBottom(btn);
        
        
        primaryStage.setScene(new Scene(root, 300, 250));
        primaryStage.show();
        
    }


    /**
     * Create an HBox of buttons
     * 
     * @return the constructed hbox
     */
    private HBox makeButtons() /*throws TwitterException*/{
        HBox hbox = new HBox();
        Button send = new Button("Send");
        send.setOnAction(event -> {
            try {
                twitterEngine.createTweet(textArea.getText());
            } catch (TwitterException ex) {
                LOG.error("Unable to send tweet", ex);
            }
        });
        Font font = new Font(18);
        send.setFont(font);

        Button exit = new Button("Exit");
        exit.setOnAction(event -> Platform.exit());
        exit.setFont(font);

        hbox.getChildren().add(send);
        hbox.getChildren().add(exit);

        hbox.setAlignment(Pos.CENTER);
        hbox.setSpacing(20.0);
        hbox.setPadding(new Insets(10, 10, 10, 10));
        return hbox;
    }
            

}
